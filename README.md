# Shell Scripts

This repository contains some scripts I wrote mostly on Bash to automate
some tasks or provide useful information. I intent to include the
documentation on the first lines of each script. If you have any
questions you can refer to this documentation.

Also feel free to also check my
[dotfiles](https://gitlab.com/damavrom/dotfiles) repository. It 
contains configuration of my setup and many scripts are relevant to this
repo.

<!--
#### newterminal.sh
[This script](newterm.sh) opens a new terminal window in the current
directory. It takes a integer as an argument to open more than one
terminals. Its purpose is to spare me from the trouble of navigating to
the directory that I am working on each time I open a new terminal.

#### prompt.sh
[This script](prompt.sh) is used by Bash shell to print the shell
prompt. Its main purpose is to print the name of the current directory
but it also may print some other interesting information. Some examples
of its use are the following.

When I am in my home directory my prompt will be:
```bash
[~]$_
```

When I am in the `/mnt` directory where the `/dev/sdb1` device is
mounted on, my prompt will be:

```bash
[mnt(/dev/sdb1)]$_
```

When I am in `home` directory that is part of the `dotfiles` git repo
then my prompt is:

```bash
[dotfiles:home]$_
```

If the name of the directory I am in is too big, then part of it is
replaces with dots:

```bash
[Develop...]$_
```

#### listdeps.sh
I wrote [this script](listdeps.sh) to figure the dependencies that are
listed in a `PKGBUILD` file that exist in the official Arch Linux
repositories and are not installed on my computer. I use it along with
`pacman` to quickly install the required dependencies. To run it, you
must first navigate to the directory where the `PKGBUILD` file is. Note
that this script is heavily dependant to Arch Linux operating system.
-->
