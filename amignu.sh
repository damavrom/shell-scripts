#!/bin/bash

# NAME
#   amigny.sh - Am I GNU?
#
# DESCRIPTION
#   This script finds and prints the percentage of the GNU packages in
#   the system. 
#
#   To calculate this, it looks if the gnu.org is the website of the
#   package or if the name belongs in the list of gnu packages.
#   Oftentimes the name is slightly different and the website is not
#   gnu.org even if the package is part of the GNU family. So the result
#   may be a little bellow the actual percentage.
#
# DEPENDENCIES
#   curl, pacman

curl -s https://www.gnu.org/software/allgnupkgs \
    --output /tmp/allgnupkgs.unformatted
cat /tmp/allgnupkgs.unformatted \
    | sed 's/<\/.*>.*$//' \
    | sed 's/<.*>//' \
    | sed '/^$/d' \
    | sort > /tmp/allgnupkgs
pacman -Qq |  sort > /tmp/allpkgs

cnt=0
have=$(cat /tmp/allpkgs | wc -l)
havegnu=$[ $(comm -12 /tmp/allpkgs /tmp/allgnupkgs | wc -l) + $cnt ]
i=$havegnu
for pkg in $(comm -23 /tmp/allpkgs /tmp/allgnupkgs); do
        if [ "$(pacman -Qi $pkg| grep URL |grep gnu.org)" != "" ];then
                cnt=$[ $cnt + 1 ]
        fi
        i=$[ $i + 1 ]
        if [ $[ i % ( $have / 89 ) ] -eq 0 ]; then
                havegnu=$[ \
                    $(comm -12 /tmp/allpkgs /tmp/allgnupkgs | wc -l) + $cnt \
                ]
                echo -n "your rig is $[ 100  * $havegnu / $have ]"
                echo -ne "%...(investigating $[ 100 * $i / $have ]%)\r"
        fi
done
echo "your rig is $[ 100  * $havegnu / $have ]% GNU                          "
rm /tmp/allgnupkgs /tmp/allpkgs /tmp/allgnupkgs.unformatted
