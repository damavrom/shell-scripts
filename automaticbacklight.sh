#!/bin/bash

# NAME
#   automaticbacklight.sh - automated backlight setter
#
# DESCRIPTION
#   This script takes photos using the webcam and adjusts then backlight
#   according to the light levels of the environment.
#
# DEPENDENCIES
#   imagemagik, xorg-xbacklight, ffmpeg

pic=/tmp/shot.png

while [ 1 ]; do
    ffmpeg $pic \
    -y \
    -f video4linux2 \
    -i /dev/video0 \
    -vframes 1 \
    -loglevel -8

    amb=$(identify -format "%[fx:mean]\n" $pic |\
    sed 's/[01]*\.\([0-9][0-9]\).*/\1/' | bc)
    #rm $pic 
    bl=$(xbacklight| sed 's/\..*//')
    if [ "$bl" -lt "$amb" ]
    then
        xbacklight + 5% -time 1000
        echo $(date +"%T") backlight: increassing 
    elif [ $[ "$bl" - 20 ] -gt "$amb" ]
    then
        xbacklight - 5% -time 1000
        echo $(date +"%T") backlight: decreassing
    fi
    diff=$[ $bl - $amb ]
    diff=${diff#-}
    sleep $[ ( 100 - $diff ) * ( 100 - $diff ) * 60 / 1000 ]
done 
rm $pic
