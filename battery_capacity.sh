#!/bin/bash

# NAME
#   battery_capacity.sh - print the percentage of the battery capacity
#
# DESCRIPTION
#   This script that prints the battery capacity percentage is usually
#   used by monitoring programs such as status bars.
#
# SYNOPSIS
#   battery_capacity.sh [charging|discharging|time|minutes]
#
# OPTIONS
#   charging
#       Output information only the battery is charging
#
#   discharging
#       Output information only the battery is discharging
#
#   time
#       Output the estimated time that the battery will be at zero
#       capacity.
#
#   minutes
#       Output the minutes of left capacity.

batDir=/sys/class/power_supply/BAT1

cap=$(cat $batDir/capacity)

[ "$1" == discharging ] \
    && [ "$(cat $batDir/status)" != "Discharging" ] \
    && echo && exit 0

[ "$1" == charging ] \
    && [ "$(cat $batDir/status)" != "Charging" ] \
    && echo && exit 0

if [ "$1" == time ]; then
    sleeptime=10
    full=$(cat $batDir/energy_full)
    a=$(cat $batDir/energy_now)
    sleep $sleeptime
    b=$(cat $batDir/energy_now)
    timeto0=0
    if [ "$a" -gt "$b" ]
    then
        timeto0=$[ $sleeptime * $b / ( $a - $b ) ]
    elif [ "$a" -lt "$b" ]
    then
        timeto0=$[ $sleeptime * ( $full - $b ) / ( $b - $a ) ]
    fi
    date +"%T" -d "now + $timeto0 sec"
    exit 0
fi

if [ "$1" == minutes ]; then
    sleeptime=10
    full=$(cat $batDir/energy_full)
    a=$(cat $batDir/energy_now)
    sleep $sleeptime
    b=$(cat $batDir/energy_now)
    timeto0=0
    if [ "$a" -gt "$b" ]
    then
        timeto0=$[ $sleeptime * $b / ( $a - $b ) ]
    elif [ "$a" -lt "$b" ]
    then
        timeto0=$[ $sleeptime * ( $full - $b ) / ( $b - $a ) ]
    fi
    echo $[ $timeto0 / 60 ]
    exit 0
fi

echo $cap%
