#!/bin/bash

# NAME
#   bluetooth_info.sh - print brief information about bluetooth status
#
# DESCRIPTION
#   This script prints whether the bluetooth device is running or it is
#   connected to a specific device.
#   This script is usually used by monitoring programs such as status bars.
#
# DEPENDENCIES
#   bluez-utils

[ "$(bluetoothctl show | awk '{if ($1 == "Powered:") print $2}')" == "no" ] \
    && echo && exit

bluetoothctl info | \
awk 'BEGIN{
        name = ""
    }{
        if ($1 == "Name:"){
            name = $2
            for (i = 3; i <= NF; i++)
                name = name" "$i
        }
    }END{
        if (name == "")
            print "on"
        else
            print name
    }'
