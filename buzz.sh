#!/bin/bash

# NAME
#   buzz.sh - a buzzer
#
# DESCRIPTION
#   This script makes a buzzing sound for a few seconds.
#
# NOTES
#   The buzz sound is the file ~/.local/share/buzz.wav
#
# DEPENDENCIES
#   alsa-utils

mpv --keep-open=no --msg-level=all=fatal --end=00:00.20 ~/.local/share/buzz.wav &
