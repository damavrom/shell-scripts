#!/bin/bash

podcasts=~/Podcasts/
raw_files=~/.cache/podcasts/raw/
feed_files=~/.cache/podcasts/feeds/
feedspath=~/.config/podcasts.conf
state=~/.cache/podcasts/state/

max_downloads=4

[ ! -f $feedspath ] && echo feedfile not present && exit 1
#init directories
mkdir -p $raw_files $feed_files $state $podcasts

#constants
one_week_ago=$(date +"%s" -d "20 days ago")
current_year=$(date +"%Y")
one_week_ago_year=$(date +"%Y" -d "a week ago")
current_month=$(date +"%b")
one_week_ago_month=$(date +"%b" -d "a week ago")
start_time=$(date +"%s")

run_id=$((date; echo $RANDOM) | md5sum | grep -o '^.....')

for feed in $(cat $feedspath | sed 's/#.*//' | grep -v '^[ \t]*$')
do
    (
        id=$(echo "$feed" | md5sum | awk '{print $1}' | grep -o '.....$')
        feed_path=$feed_files$id.rss
        download=true
        [ -f "$feed_path" ] && [ $[ $start_time-$(stat -c "%Y" $feed_path) ] -lt 3600 ] && download=false
        if $download
        then
            feed_path=/tmp/${run_id}_$id.rss
            curl -s --compressed $feed -o $feed_path
        fi
        feed_txt=$(cat "$feed_path")
        feed_title=$(
            echo "$feed_txt" \
            | tr -d '\n' \
            | sed 's/<\/item>/&\n/g' \
            | sed 's/<item>/\n&/g' \
            | sed 's/<item>.*<\/item>//g' \
            | sed 's/<image>.*<\/image>//g' \
            | grep -o '<title>.*</title>' \
            | sed 's/<!\[CDATA\[\([^]]*\)\]\]>/\1/' \
            | sed 's/<[^>]*>//g'
        )
        items=$(
            echo "$feed_txt" \
            | tr -d '\n' \
            | sed 's/<\/item>/&\n/g' \
            | sed 's/<item>/\n&/g' \
            | grep -o '<item>.*</item>' \
            | grep -o \
                -e '<item>' -e '</item>' \
                -e '<pubDate>[^<]*</pubDate>' \
                -e '<title>.*</title>' \
                -e '<enclosure[^>]*>' \
            | tr -d '\n' \
            | sed 's/<\/item>/&\n/g' \
            | grep \
                -e "$current_year" \
                -e "$one_week_ago_year" \
            | grep \
                -e "$current_month" \
                -e "$one_week_ago_month"
        )
        if [ -n "$items" ]
        then
            while read item
            do
                date=$(
                    echo "$item" \
                    | sed 's/.*<pubDate[^>]*>\([^<]*\)<\/pubDate>.*/\1/'
                )
                date_sec=$(date +"%s" -d "$date")
                if [ $one_week_ago -le "$date_sec" ]
                then
                    episode_id=$(
                        echo "$item" \
                        | md5sum \
                        | awk '{print $1}' \
                        | grep -o '.....$'
                    )
                    title=$(
                        echo "$item" \
                        | sed 's/<item>.*<title>\(.*\)<\/title>.*<\/item>/\1/' \
                        | sed 's/<!\[CDATA\[\([^]]*\)\]\]>/\1/' \
                        | sed -e 's/&apos;/'"'"'/g' \
                        | sed -e 's/&quot;/"/g' \
                        | sed -e 's/&amp;/\&/g' \

                    )
                    file_type=$(
                        echo "$item" \
                        | grep '<item>.*<enclosure [^>]*type="[^"]*".*</item>' \
                        | sed 's/<item>.*<enclosure [^>]*type="\([^"]*\)".*<\/item>/\1/'
                    )
                    if [ "$file_type" == "audio/mpeg" ] || [ "$file_type" == "video/mp4" ]
                    then
                        if [ ! -f "$state$episode_id.episode.done" ]
                        then
                            echo $feed_title - $title
                        fi
                    fi
                fi
            done <<< "$items"
        fi
        if $download
        then
            if [ -z "$(ls $state | grep -e '^'$id'\.feed\.')" ]
            then
                mv $feed_path $feed_files$id.rss
            else
                rm $feed_path
            fi
        fi
    ) &
    jobs -p > /dev/null
    [ "$(jobs -p | wc -l)" -gt $max_downloads ] && wait -n
done
wait
