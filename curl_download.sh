#!/bin/bash

[ -z "$1" ] && echo please provide a URL && exit 1
URL=$1
filename=${1##*/}
curl -C - --output $filename $URL
