#!/bin/bash

# NAME
#   dependencies.sh - print the dependencies listed in a PKGBUILD file.
#
# DESCRIPTION
#   This script prints the dependencies that exist in the official arch
#   repos and are not installed. It must be run in the directory where
#   the PKGBUILD file is located. It can also show the optional
#   dependencies.
#
# SYNOPSIS
#   dependencies.sh [optional]
#
# OPTIONS
#   optional
#       It makes the script to print the optional dependencies also.
#
# DEPENDENCIES
#   pacman

[ ! -f PKGBUILD ] && echo No PKGBUILD file in this directory && exit 1

comm -13 \
    <(pacman -Qq | sort) \
    <( \
        comm -12 \
            <(pacman -Sqs | sort) \
            <( \
                if [ "$1" == "optional" ]; then
                    cat PKGBUILD
                else
                    sed '/optdepends/, /)/ d' < PKGBUILD
                fi |\
                sed -n '/depends/, /)/ p' | \
                grep -o -e \"[^\"]*\" -e \'[^\']*\'  | \
                sed 's/"\|'\''\|: .*\|>=.*//g' | sort \
            ) \
    ) \
| awk 'BEGIN {ORS=" "} {print $0}'
echo
