#!/bin/bash

managable=$(udiskie-info --all -o '{ui_label}' | sort)
devs=$(comm -12 <(echo "$managable" | sed 's/:.*$//') <(df | awk '{print $1}' | sort))

[ -z "$devs" ] && exit

echo "$managable" | sed -n 's.'$devs': ..p'
