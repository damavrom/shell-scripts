#!/bin/bash

# NAME
#   editor.sh - text editor
#
# DESCRIPTION
#   This script opens vim editor on a urxvt emulator and passes a couple
#   of parameters. The emulator quits along with vim. It is intended to
#   be run by other programs such as web browsers.
#   On a BSPWM session, the emulator is opened in floating mode.
#
# SYNOPSIS
#   editor.sh [filename] [line] [column]
#
# OPTIONS
#   filename
#       The name of the file that the editor will edit.
#   line, column
#       The numbers that specify the position of the cursor
#
# DEPENDENCIES
#   urxvt, vim

com=${2}G${3:+${3}l}
urxvt -title "popup_terminal" -geometry 72x10 -e vim -f $1 -c "normal $com" -c "set spell" -c "set notitle"
