#!/bin/bash

# NAME
#   fancontrol.sh - fan control utility
#
# DESCRIPTION
#   This script is a simple user interface for a thinkpad fan usage. It
#   can activate or deactivate the fan depending on the CPU temperature
#   of used by a monitoring program.
#
# SYNOPSIS
#   fancotrol.sh [auto <temperature> |toggle|info|rights]
#
# OPTIONS
#   auto <temperature>
#      It turns off the fan if the CPU is cold enough and turns it on
#      when it is heated.
#
#   toggle
#       It switches the fan on and off.
#
#   info
#      If the fan is activated, print a blank line, otherwise print
#      'off'. This option is used mainly by monitoring programs.
#
#   rights
#       Cast rights to access the fan control file to everyone. This 
#       needs permissions.
#
# NOTES
#   This script needs further work.
#
# DEPENDENCIES
#   thinkfan(AUR)

fan=/proc/acpi/ibm/fan
mode=$1

if [ "$mode" == auto ]; then
    thres=${2:-50}
    high=$[ $thres * 1000 ]
    low=$[ ( $thres - 10 ) * 1000 ]
    thermalDir=/sys/class/thermal/thermal_zone0/
    while true; do
        temp=$(cat ${thermalDir}temp)
        stat=$(awk '{if ($1 == "status:") print $2 }' < $fan)
        if [ "$temp" -le $low ]; then
            [ "$stat" == "enabled" ] \
            && echo disable > $fan \
            && echo $(date +"%T") fan: disabling
        elif [ "$temp" -ge $high ]; then
            [ "$stat" == "disabled" ] \
            && echo level full-speed > $fan \
            && echo $(date +"%T") fan: enabling
        fi
        sleep 3
    done
fi

if [ "$mode" == toggle ]; then
    stat=$(awk '{if ($1 == "status:") print $2 }' < $fan)
    if [ "$stat" == "enabled" ]; then
        echo disable > $fan
    else
        echo enable > $fan
    fi
fi

if [ "$mode" == info ]; then
    stat=$(awk '{if ($1 == "status:") print $2 }' < $fan)
    if [ "$stat" == "enabled" ]; then
        echo ""
    else
        echo off
    fi
fi

[ "$mode" == rights ] && chmod 777 $fan
