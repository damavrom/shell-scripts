#!/bin/bash

# NAME
#   hacker.sh - type code like a hacker
#
# DESCRIPTION
#   This is a script inspired by hackertyper.com. It prints characters
#   from a file at the press of any key so it seems like it typed at the
#   current moment.
#
# SYNOPSIS
#   hacker.sh [filename]
#
# ARGUMENTS
#   filename
#       The path to the file to be printed.
#
# BUGS
#   There is a small chance the press character to be displayed along with the
#   intended text.

speed=2
buffsize=5
eof=false
code="$(sed -n '1, '$buffsize' p' < $1)"
s=$[ 1 + $buffsize ]

clear
while [ -n "$code" ]; do
    if [ ${#code} -lt "$[ $speed + 1 ]" ]; then
        code="$code
$(sed -n $s', '$[ $s + $buffsize - 1 ]' p' < $1)"
        s=$[ $s + $buffsize ]
    fi
    read -sn 1
    char=${code:0:$speed}
    code=${code:$speed}
    echo -n "$char"
done
read -s
