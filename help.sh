#!/bin/bash

# NAME
#   help.sh
#
# DESCRIPTION
#   This script prints the documentation that there is on the begging of
#   a specified script.
#
# SYNOPSIS
#   help.sh [script]
#
# ARGUMENTS
#   script
#       The script of which the documentation will be printed. If it is
#       left blank then it prints this documentation.

script=${1:-help.sh}
fullpath=$(which $script 2> /dev/null)

path=${fullpath%/*}

[ $? != 0 ] && echo "inappropriate argument" && exit
[ "$path" != $HOME/.local/bin ] && echo "inappropriate argument" && exit

sed -n '/^#.*NAME/, /^[^#]*$/ s/^#/ / p' < $fullpath \
