#!/bin/bash

# NAME
#   kernel_package.sh - the package of the kernel
#
# DESCRIPTION
#   It prints the name of the package of the running kernel. If the
#   package has been updated after the bootup, then it prints "not
#   known".
#
# DEPENDENCIES
#   pacman

pacman -Qoq /usr/lib/modules/$(uname -r)/kernel 2> /dev/null || echo "not known"
