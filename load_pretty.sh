#!/usr/bin/bash

# NAME
#   load_pretty.sh - output the load in a pretty format
#
# DESCRIPTION
#   This script outputs the load of the processor. It prints indicators
#   if the load is increasing or decreasing and it prints the !
#   character if the CPU runs at full speed.
#
# SYNOPSIS
#   load_pretty.sh [threshold]
#
# ARGUMENTS
#   threshold
#       If the load is less that the threshold, it only prints a blank
#       line.
#
# DEPENDENCIES
#   cpupower

threshold=${1:--1}
prnt=
[ -n "$(cpupower frequency-info -p | grep -o '"performance"')" ] && prnt=!

prnt=$prnt$(awk '{
    if ($1 > '$threshold')
        printf "%.2f", $1
    if ($1-$2 > 0.2)
        printf "%c", 0x2191
    if ($1-$2 < -0.2)
        printf "%c", 0x2193
}' < /proc/loadavg)

[ -n "$prnt" ] && echo -e "$prnt"
