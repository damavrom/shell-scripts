#!/bin/bash

# NAME
#   memory_pretty.sh - output the memory state in a pretty format
#
# DESCRIPTION
#   This script is intended to be used by monitoring programs to print
#   the memory percentage usage.
#
# SYNOPSIS
#   memory_pretty.sh [threshold]
#
# ARGUMENTS
#   threshold
#       If the percentage is less than the threshold, it doesn't print
#       anything.


threshold=${1:--1}

awk 'BEGIN{
        total = 0
        available = 0
     }{
        if ($1 == "MemTotal:")
            total += $2
        if ($1 == "MemAvailable:")
            available += $2
        if ($1 == "SwapTotal:")
            total += $2
        if ($1 == "SwapFree:")
            available += $2
     }
     END{
        per =  (1-available/total)*100
        if (per >= '$threshold')
            printf "%.0f%\n", per
     }
' < /proc/meminfo
