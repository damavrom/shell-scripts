#!/bin/bash

if [ -n "$(mpc | grep '\[playing\]')" ]
then
    song=$(mpc -f "%title%" | head -n1)
    #artists=$(mpc -f "%title%")
    [ -z "$song" ] && echo unnamed && exit 0
    [ ${#song} -gt 20 ] && echo "${song:0:17}..." && exit 0
    echo $song
fi
