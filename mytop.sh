#!/bin/bash

# NAME
#   mytop.sh - a top like program for memory monitoring
#
# DESCRIPTION
#   It prints every process along with its memory or its CPU use in an
#   descending order.
#
# SYNOPSIS
#   mytop.sh [cpu|memory] [limit]
#
# OPTIONS 
#   cpu
#       This option is used to display the cpu percentage used by ever
#       process.
#
#   memory
#       This option is used to display the memory percentage used by
#       ever process.
#
# ARGUMENTS
#   limit
#       This argument specifies the number of processes to display.

mode=${1:-cpu}
if [ -z $2 ]; then
    [ $mode == memory ] && \
        watch -t 'ps -eo comm,pmem,rss,user --sort=-pmem,-time'
    [ $mode == cpu ] && \
        watch -t 'ps -eo comm,pcpu --sort=-pcpu,-time'
else
    [ $mode == memory ] && \
        watch -t 'ps -eo comm,pmem,rss,user --sort=-pmem,-time | head -n '$2
    [ $mode == cpu ] && \
        watch -t 'ps -eo comm,pcpu --sort=-pcpu,-time | head -n '$2
fi
