#!/bin/bash

direction=rx
[ "$1" == up ] && direction=tx
ip -s -j link | sed 's/{"ifindex"\|\]$/\n&/g' |\
grep '"operstate":"UP"' |\
grep -o '"'$direction'":{[^}]*}' |\
grep -o '"bytes":[0-9]*' |\
grep -o '[0-9]*' |\
awk 'BEGIN{s = 0}{s += $1}END{print s}'
