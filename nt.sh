#!/bin/bash

# NAME
#   newterm.sh - open terminal emulators
#
# DESCRIPTION
#   It opens one or more urxvt terminal emulators in the current
#   directory.
#
# SYNOPSIS
#   newterm.sh [terminals]
#
# ARGUMENTS
#   terminals
#       Specifies the number of emulators to open.
# DEPENDENCIES
#   urxvt

i=${1:-1}
while [ $i -gt 0 ]; do
    urxvt &> /dev/null &
    i=$[ $i -1 ]
done
