#!/bin/bash

# NAME
#   orphans.sh - check for orphan pacman packages
#
# DESCRIPTION
#   This script prints the packages that are installed dependencies but
#   no explicit package requires them as dependencies.
#   It prints what `pacman -Qdtq` prints but it is also able to discover
#   circle dependencies.
#
# SYNOPSIS
#   orphans.sh [not_optional]
#
# OPTIONS
#   not_optional
#       Displays packages that may be optional dependencies of other
#       packages.
#
# DEPENDENCIES
#   pacman, pacman-contrib

pactree_cmd="pactree -u --optional=-1"
[ "$1" == "not_optional" ] && pactree_cmd="pactree -u"

investigate=$(pacman -Qdq | sort)
i=0
while [ -n "$investigate" ]
do
    pkg=$(echo "$investigate" | head -n1)
    [ -z "$(comm -12 <($pactree_cmd -r $pkg | sort) <(pacman -Qeq | sort))" ] && echo $pkg
    investigate=$(echo "$investigate" | grep -v '^'$pkg'$')
done
