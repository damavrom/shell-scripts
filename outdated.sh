#!/bin/bash

threshold=${1:--1}
db="${TMPDIR:-/tmp}/checkup-db-${UID}/"
pkgs=$(pacman -Quq --dbpath $db 2> /dev/null)
[ $? -eq 0 ] && echo n | fakeroot -- pacman -Suq --dbpath /tmp/checkup-db-$UID/ 2> /dev/null | grep 'Total Download Size' | grep -o '[0-9].*$' | awk '{
    total = $1
    if ($2 == "KiB")
        total = $1*2**10
    if ($2 == "MiB")
        total = $1*2**20
    if ($2 == "GiB")
        total = $1*2**30
    if (total > '$threshold'*2**20)
        print $1$2
}'
(
    check=true
    [ -f ${db}lastcheck ] && [ $[ $(date +"%s")-$(cat ${db}lastcheck) ] -lt 18000 ] && check=false
    $check && checkupdates &> /dev/null
    [ $? -eq 0 ] && $ckeck && date +"%s" > ${db}lastcheck
) &
