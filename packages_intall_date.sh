log=$(
    cat /var/log/pacman.log \
    | grep -e '\[ALPM\] installed' -e '\[ALPM\] removed' \
    | sed 's/(.*)//' \
    | sed 's/ \[ALPM\]//' \
    | sed 's/\[\([^ ]*\) \([^ ]*\)\]/[\1T\2]/' \
    | sort -k 3 \
    | tac \
    | uniq -f 2 \
    | grep -v ' removed ' \
    | awk '{print $1, $3}' \
    | sort -n 
)
deps=$(pacman -Qdq --color never)
for i in $deps
do
    log=$(echo "$log" | grep -v ' '$i)
done
echo "$log"
