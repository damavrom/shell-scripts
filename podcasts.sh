#!/bin/bash

# NAME
#   podcasts.sh
#
# DESCRIPTION
#   A podcast downloader. It downloads feed files, reads them, resolves
#   what episodes to download, it caches the downloads, it changes
#   metadata and it converts to .mp3 filetype if needed. It is a very
#   intricate program for the technologies it uses.
#
# NOTES
#   This script reaches the utmost potential that BASH scripting can
#   offer. It is a gem. It has a very sophisticated threading hierarchy
#   in order to parallelize downloads, information extraction, and
#   format conversions, so it makes efficient use of computing and
#   network resources. At the same time it provides info about the
#   running processes; a cumbersome function. Anything more complex
#   would require a more structure oriented language. Just because it is
#   very complex, things may go awry if this process is terminated
#   unexpectedly.
#   It won't run unless the feeds file is present.
#
# DEPENDENCIES
#   ffmpeg, curl

#needs to move these to the config file
#options
podcasts=~/Podcasts/
raw_files=~/.cache/podcasts/raw/
feed_files=~/.cache/podcasts/feeds/
feedspath=~/.config/podcasts.conf
state=~/.cache/podcasts/state/

max_downloads=2
max_processes=2

[ ! -f $feedspath ] && echo feedfile not present && exit 1
#init directories
mkdir -p $raw_files $feed_files $state $podcasts

#constants
one_week_ago=$(date +"%s" -d "20 days ago")
current_year=$(date +"%Y")
one_week_ago_year=$(date +"%Y" -d "a week ago")
current_month=$(date +"%b")
one_week_ago_month=$(date +"%b" -d "a week ago")
start_time=$(date +"%s")

[ -n "$(ls $state | grep -e '\.download$' -e '\.process$' -e '\.downloading$' -e '\.processing$')" ] && echo "state is locked" && exit 1

[ -n "$(ls $state | grep '\.error$')" ] && echo "previous execution had errors; please check and remove error files" && exit 1

start=$(date +"%s")

for feed in $(cat $feedspath | sed 's/#.*//' | grep -v '^[ \t]*$')
do
    id=$(
        echo "$feed" \
        | md5sum \
        | awk '{print $1}' \
        | grep -o '.....$'
    )
    echo id:$id > $state$id.feed.download
    echo link:$feed >> $state$id.feed.download
done
echo "-----"
#start main loop
(
while [ -n "$(ls $state | grep -e '\.download$' -e '\.process$' -e '\.downloading$' -e '\.processing$')" ] 
do
    #download next feeds in queue
    while [ -n "$(ls $state | grep '\.feed\.download$')" ] && [ $max_downloads -gt "$(ls $state | grep '\.downloading$' | wc -l)" ] 
    do
        next_file=$(ls $state | grep '\.feed\.download$' | shuf | head -n 1)
        id=${next_file%%.*}
        mv $state$id.feed.download $state$id.feed.downloading
        (
            link=$(cat $state$id.feed.downloading | sed -n 's/^link://p')
            curl -s $link -o $feed_files$id.rss
            if [ "$?" -eq 0 ]
            then
                mv $state$id.feed.downloading $state$id.feed.process
            else
                mv $state$id.feed.downloading $state$id.feed.error
                echo error:download >> $state$id.feed.error
                msg="FEED DOWNLOAD ERROR: $id, $link"
                echo "${msg:0:72}"
            fi
        ) &
    done
    
    #download next episode in queue
    while [ -n "$(ls $state | grep '\.episode\.download$')" ] && [ $max_downloads -gt "$(ls $state | grep '\.downloading$' | wc -l)" ] 
    do
        files=""
        for fl in $(ls $state | grep '\.episode\.download$')
        do
            files=$(
                echo "$files"
                echo $(sed -n 's/date://p' < $state$fl; sed -n 's/id://p' < $state$fl)
            )
        done
        id=$(echo "$files" | tail -n +2 | sort -n | head -n 1 | awk '{printf "%s", $2}')
        mv $state$id.episode.download $state$id.episode.downloading
        (
            dwn_start=$(date +"%s")
            link=$(cat $state$id.episode.downloading | sed -n 's/^link://p')
            curl -s -L "$link" --output $raw_files$id
            ret=$?
            echo "download_duration:$[ $(date +"%s") - $dwn_start ]" >> $state$id.episode.downloading
            if [ "$ret" -eq 0 ]
            then
                mv $state$id.episode.downloading $state$id.episode.process
            else
                mv $state$id.episode.downloading $state$id.episode.error
                echo error:download >> $state$id.episode.error
                length=$(cat $state$id.episode.error | sed -n  's/^length://p')
                if [ -f $raw_files$id ]
                then
                    prog=$[ 100 * $(find $raw_files -name $id -printf "%s") / $length ]
                    echo progress:$prog >> $state$id.episode.error
                fi
                title=$(cat $state$id.episode.error | sed -n  's/^title://p')
                date=$(cat $state$id.episode.error | sed -n  's/^date://p')
                timestamp=$(date -d @$date +"%y%m%d")
                feedstamp=$(cat $state$id.episode.error | sed -n  's/^feedstamp://p')
                msg="EPISODE DOWNLOAD ERROR: $(date -d @$date +"%d/%m") $feedstamp - $title                                                                        "
                echo "${msg:0:72}"
            fi
        ) &
    done

    #read next feed in queue
    while [ -n "$(ls $state | grep '\.feed\.process$')" ] && [ $max_processes -gt "$(ls $state | grep '\.processing$' | wc -l)" ] 
    do
        next_file=$(ls $state | grep '\.feed\.process$' | shuf | head -n 1)
        id=${next_file%%.*}
        mv $state$id.feed.process $state$id.feed.processing
        (
            feed_txt=$(cat "$feed_files$id.rss")
            feed_title=$(
                echo "$feed_txt" \
                | tr -d '\n' \
                | sed 's/<\/item>/&\n/g' \
                | sed 's/<item>/\n&/g' \
                | sed 's/<item>.*<\/item>//g' \
                | sed 's/<image>.*<\/image>//g' \
                | grep -o '<title>.*</title>' \
                | sed 's/<!\[CDATA\[\([^]]*\)\]\]>/\1/' \
                | sed 's/<[^>]*>//g'
            )
            feedstamp_stripped=$(
                echo "$feed_title" \
                | tr -d '[:punct:]' \
                | tr -d '[:blank:]'
            )
            feedstamp_short=${feedstamp_stripped:0:10}
            feedstamp=${feedstamp_short%_}
            echo title:$feed_title >> $state$id.feed.processing
            echo stamp:$feedstamp >> $state$id.feed.processing
            suffix_long=$(
                echo "$feed_title" \
                | tr -d '[:punct:]' \
                | tr -d '[:blank:]'
            )
            suffix_unstriped=${suffix_long:0:10}
            suffix=${suffix_unstriped%_}

            items=$(
                echo "$feed_txt" \
                | tr -d '\n' \
                | sed 's/<\/item>/&\n/g' \
                | sed 's/<item>/\n&/g' \
                | grep -o '<item>.*</item>' \
                | grep -o \
                    -e '<item>' -e '</item>' \
                    -e '<pubDate>[^<]*</pubDate>' \
                    -e '<title>.*</title>' \
                    -e '<enclosure[^>]*>' \
                | tr -d '\n' \
                | sed 's/<\/item>/&\n/g' \
                | grep \
                    -e "$current_year" \
                    -e "$one_week_ago_year" \
                | grep \
                    -e "$current_month" \
                    -e "$one_week_ago_month"
            )
            if [ -n "$items" ]
            then
                while read item
                do
                    date=$(
                        echo "$item" \
                        | sed 's/.*<pubDate[^>]*>\([^<]*\)<\/pubDate>.*/\1/'
                    )
                    date_sec=$(date +"%s" -d "$date")
                    if [ $one_week_ago -le "$date_sec" ]
                    then
                        episode_id=$(
                            echo "$item" \
                            | md5sum \
                            | awk '{print $1}' \
                            | grep -o '.....$'
                        )
                        if [ ! -f "$state$episode_id.episode.done" ]
                        then
                            good=true
                            link=$(
                                echo "$item" \
                                | grep '<item>.*<enclosure [^>]*url="[^"]*"[^>]*>.*</item>' \
                                | sed 's/<item>.*<enclosure [^>]*url="\([^"]*\)"[^>]*>.*<\/item>/\1/' \
                                | sed -e 's/&apos;/'"'"'/g' \
                                | sed -e 's/&quot;/"/g' \
                                | sed -e 's/&amp;/\&/g' \
                            )
                            title=$(
                                echo "$item" \
                                | sed 's/<item>.*<title>\(.*\)<\/title>.*<\/item>/\1/' \
                                | sed 's/<!\[CDATA\[\([^]]*\)\]\]>/\1/' \
                                | sed -e 's/&apos;/'"'"'/g' \
                                | sed -e 's/&quot;/"/g' \
                                | sed -e 's/&amp;/\&/g' \

                            )
                            file_type=$(
                                echo "$item" \
                                | grep '<item>.*<enclosure [^>]*type="[^"]*"[^>]*>.*</item>' \
                                | sed 's/<item>.*<enclosure [^>]*type="\([^"]*\)"[^>]*>.*<\/item>/\1/'
                            )
                            length=$(
                                echo "$item" \
                                | grep '<item>.*<enclosure [^>]*length="[^"]*".*</item>' \
                                | sed 's/<item>.*<enclosure [^>]*length="\([^"]*\)".*<\/item>/\1/'
                            )
                            if [ "$length" -eq 0 ]
                            then
                                length=$(curl -LIs "link" | sed -ne '/HTTP.* 200/,$p' | grep 'content-length' | awk '{print $2}')
                                if [ $? -ne 0 ]
                                then 
                                    echo "WARNING: couldn't resolve episode's length"
                                    echo "feed id: $id"
                                    echo "episode title: $title"
                                    good=false
                                fi
                            fi
                            if [ "$file_type" == "audio/mpeg" ] || [ "$file_type" == "video/mp4" ] || [ "$file_type" == "video/x-matroska" ]
                            then
                                _=
                            elif [ -z "$file_type" ] 
                            then
                                echo "WARNING: episode had no filetype specified"
                                echo "feed id: $id"
                                echo "episode title: $title"
                                good=false
                            elif [ "$file_type" != "text/markdown" ]
                            then
                                echo "WARNING: unknown type $file_type; ignoring file"
                                echo "feed id: $id"
                                echo "episode title: $title"
                                good=false
                            fi
                            if $good
                            then
                                echo link:$link > $state$episode_id.episode.download
                                echo id:$episode_id >> $state$episode_id.episode.download
                                echo feed:$feed_title >> $state$episode_id.episode.download
                                echo feedstamp:$feedstamp >> $state$episode_id.episode.download
                                echo title:$title >> $state$episode_id.episode.download
                                echo type:$file_type >> $state$episode_id.episode.download
                                echo date:$date_sec >> $state$episode_id.episode.download
                                echo length:$length >> $state$episode_id.episode.download
                            fi
                        fi
                    fi
                done <<< "$items"
            fi
            rm $state$id.feed.processing
        ) &
    done

    #process next episode in queue
    while [ -n "$(ls $state | grep '\.episode\.process$')" ] && [ $max_processes -gt "$(ls $state | grep '\.processing$' | wc -l)" ] 
    do
        next_file=$(ls $state | grep '\.episode\.process$' | shuf | head -n 1)
        id=${next_file%%.*}
        mv $state$id.episode.process $state$id.episode.processing
        ( 
            prc_start=$(date +"%s")
            title=$(cat $state$id.episode.processing | sed -n  's/^title://p')
            date=$(cat $state$id.episode.processing | sed -n  's/^date://p')
            feed=$(cat $state$id.episode.processing | sed -n  's/^feed://p')
            timestamp=$(date -d @$date +"%y%m%d")
            feedstamp=$(cat $state$id.episode.processing | sed -n  's/^feedstamp://p')
            result=${timestamp}_${feedstamp}_${id:0:3}.mp3
            echo result:$result >> $state$id.episode.processing
            if [ "$(file --mime-type -b $raw_files$id)" == "audio/mpeg" ]
            then
                ffmpeg \
                    -y \
                    -loglevel panic \
                    -i "$raw_files$id" \
                    -metadata title="$(date -d @$date +"%d/%m") - $title" \
                    -metadata album="$feed" \
                    -c copy "$podcasts$result" < /dev/null
                ret=$?
            else 
                ffmpeg \
                    -y \
                    -loglevel panic \
                    -i "$raw_files$id" \
                    -metadata title="$(date -d @$date +"%d/%m") - $title" \
                    -metadata album="$feed" \
                    -vn "$podcasts$result" < /dev/null
                ret=$?
            fi
            duration=$(ffmpeg -i $podcasts$result 2>&1 | sed -n 's/^.*Duration: \([^,]*\),.*$/\1/p' | awk 'BEGIN{
                    s = 0;
                    FS = ":";
                }{
                    s+=60*60*$1+60*$2+$3;
                }
                END{
                    print s;
                }'
            )
            echo "duration:$duration" >> $state$id.episode.processing
            echo "process_duration:$[ $(date +"%s") - $prc_start ]" >> $state$id.episode.processing
            if [ "$ret" -eq 0 ]
            then
                mv $state$id.episode.processing $state$id.episode.done
                msg="$(date -d @$date +"%d/%m") $feedstamp - $title                                                                        "
                echo "${msg:0:72}"
            else
                mv $state$id.episode.processing $state$id.episode.error
                echo error:process >> $state$id.episode.error
                msg="EPISODE DOWNLOAD ERROR: $(date -d @$date +"%d/%m") $feedstamp - $title                                                                        "
                echo "${msg:0:72}"
            fi
        ) &
    done

    w=true
    while $w
    do
        w=true
        if [ -n "$(ls $state | grep '\.download$')" ] && [ $max_downloads -gt "$(ls $state | grep '\.downloading$' | wc -l)" ] 
        then
            w=false
        fi
        if [ -n "$(ls $state | grep '\.process$')" ] && [ $max_processes -gt "$(ls $state | grep '\.processing$' | wc -l)" ] 
        then
            w=false
        fi
        if [ -z "$(ls $state | grep -e '\.download$' -e '\.process$' -e '\.downloading$' -e '\.processing$')" ] && [ -z "$(jobs -p)" ]
        then
            w=false
        fi
        $w && wait -n
    done
done
wait
) &

while [ -n "$(jobs -p)" ]
do  
    jobs > /dev/null
    prnt_str=""
    now=$(date +"%s")
    days_gone=$(echo "($now-$start)/(24*60*60)" | bc -l)

    feeds=$(ls $state | grep '\.feed\.' | wc -l)
    eps=$(ls $state | grep '\.episode\.' | grep -v '\.done$' | grep -v '\.error$' | wc -l)
    [ $feeds -gt 0 ] && prnt_str="fds:$feeds "
    [ $eps -gt 0 ] && prnt_str=$prnt_str"eps:$eps "

    total_size_eps=0
    eps=$(find $state -mtime -$days_gone -name '*episode*')
    [ -n "$eps" ] && total_size_eps=$(cat $eps 2> /dev/null | sed -n 's/length://p' | awk 'BEGIN{s=0}{s+=$1}END{printf "%d\n", s}')
    have_size_eps=$(find $raw_files -mtime -$days_gone -printf "%s\n" | awk 'BEGIN{s=0}{s+=$1}END{printf "%d\n", s}')
    if [ $total_size_eps -gt $have_size_eps ] && [ $have_size_eps -gt 0 ]
    then
        prnt_str=$prnt_str"prog:$[ 100*$have_size_eps/$total_size_eps ]% "
        if [ $have_size_eps -gt 0 ] && [ $now -ne $start ]
        then
            eta=$[ ($now-$start)*$total_size_eps/$have_size_eps-$now+$start ]
            if [ $eta -gt 120 ]
            then
                eta=$[ $eta/60 ]m 
            else
                eta=${eta}s 
            fi
            prnt_str=$prnt_str"eta:$eta "
        fi
    fi
    act=""
    for ep in $(ls $state | grep '\.episode\.downloading$')
    do
        act=$(
            txt=$(cat $state$ep 2> /dev/null)
            stt=$(stat $raw_files${ep%%.*} 2> /dev/null)
            echo "$act"
            if [ -n "$txt" ] && [ -n "$stt" ]
            then
                echo $(
                    echo "$txt" | sed -n 's/length://p'
                    echo "$stt" | sed -n 's/Size: \([0-9]*\) .*$/\1/p' 
                    echo "$txt" | sed -n 's/feedstamp://p'
                    echo "-"
                    echo "$txt" | sed -n 's/title://p'
                )
            fi
        )
    done
    act=$(echo "$act" | tail -n +2 | awk '{printf "%d%% %s\n", 100*$2/$1, $0}' | sort -nrk1 | sed 's/^\([^ ]*\) [0-9]* [0-9]* /dwnld:(\1) /' | head -n 1)
    if [ -z "$act" ]
    then
        eps=$(find $state -name '*.episode.processing')
        [ -n "$eps" ] && act=$(cat "$eps" 2> /dev/null | sed -n 's/title:/prcs: /p')
    fi
   #if [ -z "$act" ]
   #then
   #    for feed in $(ls $state | grep '\.feed\.downloading$')
   #    do
   #        act=$(
   #            txt=$(cat $state$feed 2> /dev/null)
   #            stt=$(stat $state$feed -c %Z 2> /dev/null)
   #            echo "$act"
   #            if [ -n "$txt" ] && [ -n "$stt" ]
   #            then
   #                echo $(
   #                    echo "$stt"
   #                    echo "$txt" | sed -n 's/link://p'
   #                )
   #            fi
   #        )
   #    done
   #    act=$(echo "$act" | tail -n +2 | sort -nk1 | sed 's/^[0-9]* /feed:/' | sed 's/https:\/\///' | head -n 1)
   #fi
    prnt_str=$prnt_str$act

    prnt_str=$prnt_str"                                                                             "
    echo -ne "${prnt_str:0:72}\r"
    sleep 5
    echo > /dev/null
done
echo -ne "                                                                              \r"

now=$(date +"%s")
days_gone=$(echo "($now-$start)/(24*60*60)" | bc -l)

#print final info
total_feeds=$(find $feed_files -mtime -$days_gone | wc -l)
total_feeds_size=$(find $feed_files -mtime -$days_gone -printf "%s\n" | awk 'BEGIN{s=0}{s+=$1}END{print s}')
total_episodes=$(find $state -mtime -$days_gone -name '*.episode.done' | wc -l)
total_episodes_size=$(find $raw_files -mtime -$days_gone -printf "%s\n" | awk 'BEGIN{s=0}{s+=$1}END{print s}')

total_size=$[ $total_episodes_size + $total_feeds_size ]
total_size_print=""
if [ $total_size -gt $[ 2 ** 21 ] ]
then
    total_size_print=$[ $total_size / ( 2 ** 20 ) ]MiB
else
    total_size_print=$[ $total_size / ( 2 ** 10 ) ]KiB
fi
total_time=$[ $now - $start ]
total_time_print=""
if [ $total_time -gt 120 ]
then
    total_time_print="$[ $total_time / 60 ]' $[ $total_time % 60 ]\""
else
    total_time_print=$total_time\"
fi
speed=$[ $total_size / $total_time ]
speed_print=""
if [ $speed -gt $[ 2 ** 21 ] ]
then
    speed_print=$[ $speed / ( 2 ** 20 ) ]MiB/s
else
    speed_print=$[ $speed / ( 2 ** 10 ) ]KiB/s
fi

listening_time=""
eps=$(find $podcasts -mtime -$days_gone -name '*.mp3')
for ep in $eps
do
    listening_time=$(
        echo "$listening_time"
        ffmpeg -i $ep 2>&1 | sed -n 's/^.*Duration: \([^,]*\),.*$/\1/p'
    )
done
listening_time_print=$(echo "$listening_time" | tail -n +2 | awk '
    BEGIN{
        s = 0;
        FS = ":";
    }{
        s+=60*60*$1+60*$2+$3;
    }
    END{
        if (s > 2*60*60)
            printf "%dhrs ", s/(60*60);
        else if (s > 60*60)
            printf "1hr ";
        if (s%(60*60) > 2*60)
            printf "%dmins ", (s%(60*60))/60;
        else if (s%(60*60) > 60)
            printf "1min ";
        if (s%60 > 1)
            printf "%dsecs", s%60;
        else if (s%60 == 1)
            printf "1sec ";
    }
')
listening=0
cnt=0
oldest=$now
for ep in $(ls $state | grep '\.episode\.done$')
do
    date=$(cat $state$ep | sed -n 's/^date://p')
    [ $date -lt $oldest ] && oldest=$date
    if [ $date -gt $(date +"%s" -d "now - 30 days") ]
    then
        cnt=$[ $cnt + 1 ]
        duration=$(cat $state$ep | sed -n 's/^duration://p')
        listening=$(echo $listening + $duration | bc -l)
    fi
done
dur=$[ $now - $(date +"%s" -d "now - 30 days") ]
[ $(date +"%s" -d "now - 30 days") -lt $oldest ] && dur=$[ $now - $oldest ]
listening=$(echo 60*60*24*$listening/$dur | bc -l | awk '{
    s = $1;
    if (s > 2*60*60)
        printf "%dhrs ", s/(60*60);
    else if (s > 60*60)
        printf "1hr ";
    if (s%(60*60) > 2*60)
        printf "%dmins ", (s%(60*60))/60;
    else if (s%(60*60) > 60)
        printf "1min ";
    if (s%60 > 1)
        printf "%dsecs", s%60;
    else if (s%60 == 1)
        printf "1sec ";
}')
cnt=$(echo "scale = 1; 60*60*24*$cnt/$dur" | bc -l)

[ $total_episodes -gt 0 ] && echo "-----"
echo                              "total feeds:      $total_feeds"
[ $total_episodes -gt 0 ] && echo "total episodes:   $total_episodes"
echo                              "total size:       $total_size_print ($speed_print)"
echo                              "running time:     $total_time_print"
[ -n "$listening_time" ] && echo  "total duration:   $listening_time_print"
echo "-----"
echo                              "per day episodes: $cnt"
echo                              "per day duration: $listening"
echo "-----"

#delete old episodes
rm $(find $raw_files -mtime +7 -type f) 2> /dev/null
rm $(find $feed_files -mtime +7 -name '*.rss') 2> /dev/null
rm $(find $podcasts -mtime +30 -name '*.mp3') 2> /dev/null
rm $(find $state -mtime +90 -type f) 2> /dev/null
