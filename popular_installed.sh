#!/bin/bash

# NAME
#   popular_installed.sh - output installed packages sorted by
#   popularity
#
# DESCRIPTION
#   This script outputs all the exclusively installed packages along
#   with their popularity and it sorts it accordingly. The data are
#   acquired from the pkgstats results.
#
# DEPENDENCIES
#   curl, jq, pacman

popularities=$(
    curl -s 'https://pkgstats.archlinux.de/api/packages?limit=0' |
    jq -c '.packagePopularities[]'
)
m=0
for i in $(pacman -Qeq); do
    [ ${#i} -gt $m ] && m=${#i}
done
for i in $(pacman -Qeq); do
    echo -n "$i "
    pop=$(
        echo "$popularities" |
        grep '"'$i'"' |
        sed 's/.*"popularity":\([^,]*\),.*/\1/'
    )
    [ -z "$pop" ] &&
        pop=$(
            curl -s 'https://pkgstats.archlinux.de/api/packages/'$i |
            sed 's/.*"popularity":\([^,]*\),.*/\1/'
        )
    echo $pop
done | sort -rnk 2 |
awk '
    BEGIN{
        printf "%-'$m's %s\n", "package", "popularity"
    }{
        printf "%-'$m's %s\n", $1, $2 
    }
'
