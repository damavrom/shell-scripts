#!/bin/bash

# NAME
#   popular_not_installed.sh - output not installed packages sorted by
#   popularity
#
# DESCRIPTION
#   This script outputs the most popular packages that are not installed
#   along with their popularity and it sorts it accordingly. The data
#   are acquired from the pkgstats results.
#
# SYNOPSIS
#   popular_not_installed.sh [limit]
#
# ARGUMENTS
#   limit
#       It specifies the number of packages to output. The default is
#       100.
#
# DEPENDENCIES
#   curl, jq, pacman

limit=${1:-100}
limit_jq=$[ "$(pacman -Qq | wc -l)" + $limit ]
popularities=$(
    curl -s 'https://pkgstats.archlinux.de/api/packages?limit=0' |
    jq -c '.packagePopularities[]' |
    sed 's/.*"name":\("[^"]*"\).*"popularity":\([^,]*\),.*/\1 \2/' |
    sort -rnk 2 |
    head -n $limit_jq
)
for i in $(pacman -Qq); do
    popularities=$(
        echo "$popularities" |
        grep -v '"'$i'"'
    )
done

popularities=$(
    echo "$popularities" |
    head -n $limit |
    tr -d '"'
)
m=0
for i in $(echo "$popularities" | awk '{print $1}'); do
    [ ${#i} -gt $m ] && m=${#i}
done
echo "$popularities" |
awk '
    BEGIN{
        printf "%-'$m's %s\n", "package", "popularity"
    }{
        printf "%-'$m's %s\n", $1, $2 
    }
'
