#!/usr/bin/bash

# NAME
#   promt.sh - a promt string
#
# DESCRIPTION
#   This script prints the prompt for terminal shells. Its main purpose
#   is to print the name of the current directory but it also may print
#   some other interesting information like the name of the git
#   repository or the filesystem if the directory is a mountpoint.
#
# NOTES
#   It is slower than a non dynamic promt string.
#
# DEPENDENCIES
#   git

# Check if it home
[ "$PWD" == "$HOME" ] && echo -n "~" && exit 0

# print working directory
d=${PWD##*/}

# shorten name of directory if big
[ ${#d} -gt 10 ] && d="${d:0:7}..."

# check if directory is mount point
filesystem=$(sed -n '\: '"$PWD"' : s: .*:: p'< /proc/mounts)

# check if is in a git repo
repo=$(git remote -v 2> /dev/null | sed -n -e 's/.*\///' -e '1 s/\.git.*// p')
if [ -n "$repo" ]
then
    # shorten name of repo if big
    [ ${#repo} -gt 10 ] && repo="${repo:0:7}..."

    # first print the repo name in bold
    tput bold
    echo -n $repo
    tput sgr0
    echo -n :
fi
	
# print the rest of the results
echo -n $d
[ -n "$filesystem" ] && echo -n "($filesystem)"
#echo -n " - $(date +"%H:%M")"
