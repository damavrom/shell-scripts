#!/bin/bash

# NAME
#   qrcode_viewer.sh - view a QR code
#
# DESCRIPTION
#   This script generates a QR code and displayes it. It is intended to
#   be run by other programs such as web browsers. On a BSPWM session,
#   the emulator is opened in floating mode.
#
# SYNOPSIS
#   qrcode_viewer.sh [text] [timeout]
#
# ARGUMENTS
#   text
#       The text to encode.
#   timeoute
#       The time to show the QR code before turning it off.
#
# DEPENDENCIES
#   qrencode, sxivd

qrencode "$1" -o /tmp/qr.png -s 10
[ $(which bspc 2> /dev/null) ] \
    && [ $(pgrep bspwm) ] && bspc rule -a "Sxiv" -o state=floating
[ -n "$2" ] && timeout $2 sxiv -g "290x290" -b /tmp/qr.png
[ -z "$2" ] && sxiv -g "290x290" -b /tmp/qr.png
rm /tmp/qr.png
