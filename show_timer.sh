#!/bin/bash

ps -C sleep -o etimes,args --sort=etime --no-headers | sed -n 's/[0-9][0-9]*/ & /gp' | awk '{
    if (NF > 4)
        next
    time = $3
    if ($4 == "m")
        time *= 60
    else if ($4 == "h")
        time *= 60*60
    else if ($4 == "d")
        time *= 60*60
    else if ($4 != "" && $4 != "s")
        next
    if (time < 5*60)
        next
    left = time-$1
    if (left >= 2*60*60)
        printf "%dhrs ", left/(60*60);
    else if (left > 60*60)
        printf "1hr ";
    if (left >= 2*60)
        printf "%dmins ", (left%(60*60))/60;
    else if (left >= 60)
        printf "1min ";
    else if (left > 60 && left%(60*60) < 60)
        printf "0min ";
    if (left < 3*60) {
        if (left%60 == 1)
            printf "1sec ";
        else
            printf "%dsecs", left%60;
    }
    print ""
    exit
}'

