#!/bin/bash

comm='n=5; echo -n "suspending in... "; for i in $(seq 1 $n); do sleep 1; echo -n "$[ $n-$i ]... "; done; systemctl suspend'
urxvt -title "popup_terminal" -geometry 72x10 -e bash -c "$comm"
