## Suspended Scripts

This directory contains all the scripts that I don't want to use for one
reason or the other but yet I don't want to completely remove.

Beware, the code in this directory is more likely to be bloated and hard
to interpret by people.
