#!/bin/bash

# It beeps when the load is high
# NOTE: This scripts needs the hardware beep to be working

while true
do
	load=$(cat /proc/loadavg|awk {'print $1'}|sed s/\\.//)
	if [ $load -gt 400 ]
	then
		beep -l 500
	elif [ $load -gt 300 ]
	then
		if [ $[ $(date +"%S"|bc) % 5 ] -eq 0 ]
		then
			beep -l 50 &
		fi
	fi
	sleep 1
done
