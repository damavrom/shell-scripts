#!/bin/bash

# It is an attempt to make the wallpaper look alive and correspond 
# according the content of the desktop
# NOTE: It is slow and heavy, hence a failure
# NOTE: It depends on hsetroot and ImageMagick

max_diff=10

to_set=$(convert /tmp/screen.png -resize 1x1 txt:- | tail -n 1 | awk {'print $3'})
hsetroot -solid $to_set
scrot -z /tmp/screen.png
temp=$(convert /tmp/screen.png -resize 1x1 txt:- | tail -n 1 | awk {'print $4'})
temp=${temp#*(}
temp=${temp%)}
temp=$(echo $temp | sed 's/,/ /g')
old_r=$(echo $temp |awk {'print $1'})
old_g=$(echo $temp |awk {'print $2'})
old_b=$(echo $temp |awk {'print $3'})

while [ 1 ];do
    scrot -z /tmp/screen.png
    temp=$(convert /tmp/screen.png -resize 1x1 txt:- | tail -n 1 | awk {'print $4'})
    temp=${temp#*(}
    temp=${temp%)}
    temp=$(echo $temp | sed 's/,/ /g')
    r=$(echo $temp |awk {'print $1'})
    g=$(echo $temp |awk {'print $2'})
    b=$(echo $temp |awk {'print $3'})
    if [ $[ $r - $old_r ] -gt $max_diff ];then
        new_r=$[ $old_r + $max_diff ]
    elif [ $[ $r - $old_r ] -lt $[ 0 - $max_diff ] ]; then
        new_r=$[ $old_r - $max_diff ]
    else
        new_r=$r
    fi

    if [ $[ $g - $old_g ] -gt $max_diff ];then
        new_g=$[ $old_g + $max_diff ]
    elif [ $[ $g - $old_g ] -lt $[ 0 - $max_diff ] ]; then
        new_g=$[ $old_g - $max_diff ]
    else
        new_g=$g
    fi

    if [ $[ $b - $old_b ] -gt $max_diff ];then
        new_b=$[ $old_b + $max_diff ]
    elif [ $[ $b - $old_b ] -lt $[ 0 - $max_diff ] ]; then
        new_b=$[ $old_b - $max_diff ]
    else
        new_b=$b
    fi

    printf -v hex_r "%x" $new_r
    if [ ${#hex_r} -lt 2 ];then
        hex_r=0$hex_r
    fi
    printf -v hex_g "%x" $new_g
    if [ ${#hex_g} -lt 2 ];then
        hex_g=0$hex_g
    fi
    printf -v hex_b "%x" $new_b
    if [ ${#hex_b} -lt 2 ];then
        hex_b=0$hex_b
    fi
    montage ~/.local/share/stamp.png -background "#$hex_r$hex_g$hex_b" -geometry 1920x1080 ~/.local/share/wallpaper.png 2> /dev/null
    hsetroot -center ~/.local/share/wallpaper.png

    echo $hex_r $hex_g $hex_b

    old_r=$new_r
    old_g=$new_g
    old_b=$new_b
    sleep 2

done
