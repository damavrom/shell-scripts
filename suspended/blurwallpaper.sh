#!/bin/bash

# It blurs the wallpaper if the desktop is dirty.
# NOTE: The blurred wallpaper must be blurred beforehand with an image
# processing tool and placed along with the actual wallpaper in the
# ~/.local/share/ folder with the names wallpaper and wallpaper-blur
# NOTE: This script depends on hsetroot and BSPWM
# WARNING: This script malfunctions

hsetroot -fill ~/.local/share/wallpaper
clean=true
[ -n "$(bspc query -N -d focused)" ] && clean=false
while true; do
	bspc subscribe | read
	if $clean
	then
		if [ -n "$(bspc query -N -d focused)" ]
		then
			hsetroot -fill ~/.local/share/wallpaper-blur
			clean=false
		fi
	else
		if [ -z "$(bspc query -N -d focused)" ]
		then
			hsetroot -fill ~/.local/share/wallpaper
			clean=true
		fi
	fi
done
