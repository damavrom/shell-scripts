#!/bin/bash

# It briefly prints the hours that the system is up.

s=$(date +"%s" -d "$(uptime -s)")
secup=$[ "$(date +"%s")" - "$s" ]
hoursup=$[ $secup / (60 * 60) ]
echo ${hoursup}hrs
