#!/bin/bash
sleeptime=10
full=$(cat /sys/class/power_supply/BAT1/energy_full)
while [ 1 ]
do
    cur=$(cat /sys/class/power_supply/BAT1/energy_now)
    per=$(echo 'scale=5; '$cur' / '$full | bc)
	echo $(date +%s) $per >> ~/battery.log
	echo $per
	sleep $sleeptime
done
