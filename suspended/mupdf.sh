#! /bin/bash

# It opens a pdf file in a new clean desktop
# NOTE: This script depends on BSPWM and MuPDF

i=1
while [ "$(bspc query -N -d "^$i")" != "" ]
do
	i=$[ $i + 1 ]
	if [ $i -gt 10 ]
	then
		i=6
		break;
	fi
done
bspc rule -a MuPDF -o desktop="^$i" state=pseudo_tiled follow=on
mupdf "$1" 
#mupdf -C fffad0 "$1" 
