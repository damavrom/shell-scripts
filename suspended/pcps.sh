#!/bin/bash
sleeptime=5
totaltime=60
full=$(cat /sys/class/power_supply/BAT1/energy_full)
echo $(cat /sys/class/power_supply/BAT1/energy_now) > /tmp/battery
while [ 1 ]
do
	sleep $sleeptime
	echo $(cat /sys/class/power_supply/BAT1/energy_now) >> /tmp/battery
	tail -n $[ $totaltime / $sleeptime ] /tmp/battery > /tmp/temp
	mv /tmp/temp /tmp/battery
	a=$(head -n 1 /tmp/battery)
	b=$(tail -n 1 /tmp/battery)
	c=$[ ( $b - $a ) * 1000000 / ( $full * ( $(cat /tmp/battery |wc -l) - 1) * $sleeptime ) ]
	echo $c
done
