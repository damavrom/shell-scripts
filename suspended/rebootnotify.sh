#!/bin/bash

# It notifies for reboot if the kernel has been updated after the system
# was booted
# NOTE: This script is Arch Linux dependant 
# NOTE: The package of the kernel is set in this code, the script is 
# oblivious of the running kernel

lku=$(grep " linux "  /var/log/pacman.log |tail -n 1 |awk '{print $1" "$2}' | cut -d "[" -f2 | cut -d "]" -f1)
lb=$(uptime -s)
lb=${lb%:*}
if [ "$lb" \< "$lku" ]
then
	echo "Reboot needed"
	echo
fi
