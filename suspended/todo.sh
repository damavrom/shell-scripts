#!/bin/bash

todofile=~/.calcurse/todo

[ ! -s $todo ] && echo && exit

sed -n '1 s/[^ ]* // p' < $todofile
