#!/bin/bash

now=$(date +"%s")
boot=$(date +"%s" -d "$(uptime -s)")
hoursup=$[ ( $now - $boot ) / ( 60 * 60 ) ]
echo $[ 1000 - $hoursup ]
