#!/bin/bash

# NAME
#   timelapse.sh
#
# DESCRIPTION
#   A script to automate the building of timelapse videos out of photos
#   I take with my Kodak. It first downsizes all the frames so the video
#   won't be very heavy. Then it puts everything into a video that
#   places on the current working directory.
#
# NOTES
#   For now it only supports photos with 100_%04d.JPG filename format.
#   This is the filename format my camera uses.
#
# SYNOPSIS
#   timelase.sh [first_frame] [last_frame] [resolution] [framerate]
#
# ARGUMENTS
#   first_frame
#       The path to the image file that is to be the first frame of the video
#   last_frame
#       The path to the image file that is to be the last frame of the video
#
# OPTIONS
#   resolution (optional)
#       The resolution of the output video. Default is 1920(HD).
#   framerate (optional)
#       The framerate of the output video. Different framerate results
#       to different video length. Default is 24 frames/second.
#
# DEPENDENCIES
#   ffmpeg, imagemagick

cache_dir=~/.cache/timelapse

[ -n "$1" ] && first_frame=$1
[ -z "$1" ] && echo please give path to first frame && exit 1

[ -n "$2" ] && last_frame=$2
[ -z "$2" ] && echo please give path to last frame && exit 1

size=${3:-1920}
fr=${4:-24}

dir=${first_frame%/*}
[ -z $(echo $first_frame | grep -o '/') ] && dir="."

### WARNING: assumes 100_%04d.JPG filename format
first_filename=${first_frame##*/}
first_number=$(echo $first_filename | sed 's/^100_\|\.JPG//g' | bc)

last_filename=${last_frame##*/}
last_number=$(echo $last_filename | sed 's/^100_\|\.JPG//g' | bc)

[ -d $cache_dir ] && rm -r $cache_dir
mkdir -p $cache_dir

total_frames=$[ $last_number - $first_number ] 
cnt=1

for number in $(seq $first_number $last_number)
do
    ### WARNING: assumes 100_%04d.JPG filename format
    filename=$dir/100_$(printf '%04d' $number).JPG
    if [ -f $filename ]; then
        [ $(jobs -p | wc -l) -ge 4 ] && wait -n
        echo -ne "downsizing frames ($[ 100 * $cnt / $total_frames ]%)\r"
        #convert $filename -rotate 180 -resize ${size}x$size $cache_dir/frame_$(printf '%04d' $cnt).jpg &
        convert $filename -resize ${size}x$size $cache_dir/frame_$(printf '%04d' $cnt).jpg &
        cnt=$[ $cnt + 1 ]
    fi
done
echo "downsizing frames done  "
ffmpeg -y -loglevel 16 -framerate $fr -i $cache_dir/frame_%04d.jpg -c:v libx264 -pix_fmt yuv420p ./timelapse.mp4
rm -r $cache_dir
echo total frames  : $cnt
echo video duration: $[ $cnt / $fr ]s
echo video size    : $(ls -lh ./timelapse.mp4 | awk '{print $5}')
