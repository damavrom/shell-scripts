#!/bin/bash

# NAME
#   torrent-info.sh - output information regarding torrents
#
# DESCRIPTION
#   If transmission daemon is on it prints the minimum percentage of
#   torrents. If they are all more than 100%, then it prints `on`.
#
# DEPENDENCIES
#   transmission-remote


transmission-remote -si > /dev/null 2>&1
[ "$?" -ne 0 ] && echo && exit

per=$(
transmission-remote -l 2> /dev/null|\
awk '{print $2}'|\
sed -e 's/[^0-9]//g' -e '/^[:space"]*$\|....*/ d'|\
sort -n |\
head -n 1)
[ -n "$per" ] && echo $per% || echo on
