#!/bin/bash

# NAME
#   unsynchronised_git.sh - check for unsynchronised git repositories
#
# DESCRIPTION
#   Looks in the ~/git directory for all the repositories that are out
#   of sync with their remotes.
#
# DEPENDENCIES
#   git

git_dir=~/git
cd $git_dir
for obj in *; do
    cd $git_dir
    [ ! -d "$obj" ] && echo         "not directory:           $obj" && continue
    cd "$obj"
    git status &> /dev/null
    [ $? -ne 0 ] && echo            "not repository:          $obj" && continue
    git fetch &>  /dev/null
    [ $? -ne 0 ] && echo            "error fetching:          $obj" && continue
    remote_branch=$(git remote | head -n 1)
    current_branch=$(git branch --show-current)
    [ -z "$remote_branch" ] && echo "no remote repository:    $obj" && continue
    last_local=$(git rev-list -n 1 $current_branch)
    last_remote=$(git rev-list -n 1 $remote_branch/$current_branch)
    [ "$last_local" != "$last_remote" ] \
        && echo                     "out of sync:             $obj"
done
