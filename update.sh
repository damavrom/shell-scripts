#!/bin/bash

# NAME
#   update.sh - update the system
#
# DESCRIPTION
#   This script is a wrapper for pacman that it first downloads all
#   packages to update and then ask the user for confirmation. It may
#   also ignore the kernel package so the system won't need a reboot.
#   This script is intended to be used in scenarios when the internet
#   connection is very unstable and it may interrupt the execution of
#   `pacman -Syu`, it will continually run until all packages are
#   downloaded.
#
# DEPENDENCIES
#   pacman

pacman -Syuw --noconfirm
while [ $? -ne 0 ]
do
	sleep 5;
	pacman -Syuw --noconfirm
done
[ -z "$(pacman -Qu | sed '/ignored/ d')" ] && exit
ignore=$(pacman -Qqo /usr/lib/modules/$(uname -r)/kernel 2> /dev/null)
if [ -n "$ignore" ]; then
    pacman -Su --ignore $ignore
else
    pacman -Su
fi
