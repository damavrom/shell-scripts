#!/bin/bash

while [ "$(sed -e 's/\.\| .*//g' -e 's/^0//' < /proc/loadavg)" -gt 50 ]; do
    sleep $[ $RANDOM % 300 ]
done
