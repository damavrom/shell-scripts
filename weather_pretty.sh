#!/bin/bash

# NAME
#   weather_pretty.sh - pretty weather information
#
# DESCRIPTION
#   This script prints weather information intended to be used by
#   monitoring software. It may print in a quiet or a verbose mode.
#
# SYNOPSIS
#   weather_pretty.sh [toggle]
#
# OPTIONS
#   toggle
#       Toggle verbosity for the current and the next executions.
#
# DEPENDENCIES
#   weather(AUR)

location=lgts
[ ! -f /tmp/weather ] && weather-report -mq $location > /tmp/weather 2> /dev/null

[ -n "$(grep verbose /tmp/weather)" ] && verbose=1 || verbose=0

if [ "$1" == "toggle" ]; then
    [ $verbose -eq 1 ] && verbose=0 || verbose=1
fi

last_output=$(cat /tmp/weather_output 2> /dev/null)
temp=$( grep -i "temperature" /tmp/weather | awk {'print $2"°C"'})
[ -n "$temp" ] && echo -n "$temp" > /tmp/weather_output

w=$(grep -i "weather" /tmp/weather| tr '[:upper:]' '[:lower:]')
c=$(grep -i "sky conditions" /tmp/weather| tr '[:upper:]' '[:lower:]')
if [ -n "$w" ];then
    w=${w#*: }
    w=${w%,*}
    w=${w%;*}
    echo -n " $w" >> /tmp/weather_output
elif [ -n "$c" ];then
    c=${c#*: }
    c=${c%,*}
    c=${c%;*}
    echo -n " $c sky" >> /tmp/weather_output
fi

[ "$last_output" != "$(cat /tmp/weather_output 2> /dev/null)" ] && verbose=0

if [ $verbose -eq 1 ]; then
    cat /tmp/weather_output 2> /dev/null
    echo
else 
    awk {'print $1'} < /tmp/weather_output 2> /dev/null
fi

{
    weather-report -mq $location > /tmp/weather 2> /dev/null
    [ $verbose -eq 1 ] && echo verbose >> /tmp/weather
} &
